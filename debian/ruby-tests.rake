require 'gem2deb/rake/spectask'

Gem2Deb::Rake::RSpecTask.new do |spec|
  spec.pattern = './spec/**/*_spec.rb'
  # skip the perf tests due to mising test dependencies
  spec.exclude_pattern = [
    './spec/perf/size_spec.rb',
  ].join ','
end

